import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3
import QtGraphicalEffects 1.0
import Ubuntu.Components.Popups 1.3
import "../components"

ListItem {
    id: cardListItem

    height: layout.height

    selectMode: stackSettingsPage.selectMode

    onPressAndHold: {
        stackSettingsPage.selectMode = true
        selected = true
    }
    readonly property string id: front

    Connections {
        target: stackSettingsPage
        onSelectAll: selected = true
    }

    onSelectedChanged: {
        if (selected) stackSettingsPage.selectedList[id] = back
        else delete stackSettingsPage.selectedList[id]
    }

    onClicked: {
        if (stackSettingsPage.selectMode) {
            selected = !selected
        }
        else {
            activeCardFront = front
            PopupUtils.open( createCardDialog )
        }
    }

    ListItemLayout {
        id: layout
        width: parent.width
        title.text: front
        title.color: settings.darkMode ? "white" : "black"

        Icon {
            name: "stock_note"
            SlotsLayout.position: SlotsLayout.Leading
            width: units.gu(4)
            height: width
        }
    }

    // Delete Button
    leadingActions: ListItemActions {
        actions: [
        Action {
            iconName: "edit-delete"
            onTriggered: {
                activeCardFront = front
                PopupUtils.open( removeCardDialog )
            }
        }
        ]
    }

}
